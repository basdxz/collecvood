package com.gitlab.basdxz.collecvood.event;

import com.gitlab.basdxz.collecvood.Reference;
import thebetweenlands.common.entity.mobs.EntityWight;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@EventBusSubscriber(modid= Reference.MODID)
public class LivingDamageEventHandler {
    @SubscribeEvent
    public void damageEvent(LivingDamageEvent event) {
        if (event.getEntityLiving() instanceof EntityWight) {
            Entity damageSource = event.getSource().getTrueSource();
            if (damageSource instanceof EntityPlayer) {
                ItemStack weaponUsed = ((EntityPlayer) damageSource).getHeldItemMainhand();
                if (weaponUsed.isItemEqual(new ItemStack(Item.getByNameOrId(Reference.MODID + ":" + Reference.ICE_AXE_STRING)))) {
                    event.setAmount(event.getAmount() * 10);
                    return;
                }
            }
            event.setCanceled(true);
        }
    }
}
