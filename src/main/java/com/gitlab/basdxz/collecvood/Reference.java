package com.gitlab.basdxz.collecvood;

public class Reference {
    public static final String MODID = "collecvood";
    public static final String MODNAME = "Collec Vood";
    public static final String VERSION = "${version}";
    public static final String ACCEPTED_MINECRAFT_VERSIONS = "[1.12.2]";
    public static final String DESCRIPTION = "Finest in Vood Collection!";
    public static final String AUTHOR = "basdxz";
    public static final String LOGO = "assets/collecvood/textures/items/iceaxe.png";
    public static final String ICE_AXE_STRING = "iceaxe";
}
