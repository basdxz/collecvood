package com.gitlab.basdxz.collecvood.item;

import com.gitlab.basdxz.collecvood.Reference;
import net.minecraft.item.ItemPickaxe;

public class IceAxeItem extends ItemPickaxe {
    public IceAxeItem(String name) {
        super(ToolMaterial.IRON);
        setUnlocalizedName(Reference.MODID + "." + name);
        setRegistryName(name);
    }
}
