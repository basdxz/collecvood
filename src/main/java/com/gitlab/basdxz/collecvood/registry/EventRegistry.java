package com.gitlab.basdxz.collecvood.registry;

import com.gitlab.basdxz.collecvood.event.LivingDamageEventHandler;
import net.minecraftforge.common.MinecraftForge;

public class EventRegistry {
    public static void init() {
        MinecraftForge.EVENT_BUS.register(new LivingDamageEventHandler());
    }
}
